﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using N09.Models;
using System.ComponentModel.DataAnnotations;
namespace N09.Controllers
{
    public class KhachHangController : Controller
    {
        // GET: KhachHang
        public ActionResult Danhsach()
        {
            //1. Lay danh sach du lieu bang
            testEntities3 db = new testEntities3();
            List<KhachHang> danhSachKhachHang = db.KhachHangs.ToList();
            return View(danhSachKhachHang);
        }
        public ActionResult ThemMoi() 
        {
            return View();
        }
        [HttpPost]
        public ActionResult ThemMoi(KhachHang model)
        {
            //Them moi ban ghi
            testEntities3 db = new testEntities3();
            db.KhachHangs.Add(model);
            //luu lai thay doi
            db.SaveChanges();
            return RedirectToAction("Danhsach");
        }
        public ActionResult CapNhat(int id) 
        {
            //3. TIm doi tuong theo ID
            testEntities3 db = new testEntities3();
            //Khachhang model1 = db.KhachHangs.SingleOrDefault(m => m.ID == id);
            KhachHang model2 = db.KhachHangs.Find(id);

            return View(model2);
        }

        [HttpPost]
        public ActionResult CapNhat(KhachHang model) 
        {
            testEntities3 db = new testEntities3();
            //tim doi tuong
            var updateModel = db.KhachHangs.Find(model.ID);
            //gan gia tri
           
            updateModel.SoDienThoai = model.SoDienThoai;
            updateModel.TenKhachHang = model.TenKhachHang;
            updateModel.DiaChi = model.DiaChi;
            //luu thay d
            db.SaveChanges();
            return RedirectToAction("Danhsach");
        }
        public ActionResult Xoa(int id)
        {
            testEntities3 db = new testEntities3();
            //tim doi tuong
            var updateModel = db.KhachHangs.Find(id);
            //Lenh xoa
            db.KhachHangs.Remove(updateModel);
            db.SaveChanges();
            return RedirectToAction("Danhsach");
        }
    }
}